package inc.minuth.recyclerviewdemo.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import javax.security.auth.callback.Callback;

import inc.minuth.recyclerviewdemo.CallBack.CallBackMovie;
import inc.minuth.recyclerviewdemo.Entity.Movie;
import inc.minuth.recyclerviewdemo.R;

public class AdapterMovie extends RecyclerView.Adapter<AdapterMovie.MyViewHolder> {

    List<Movie> movieList;
    Context context;
    LayoutInflater inflater;
    CallBackMovie callBackMovie;

    public AdapterMovie(List<Movie> movieList, Context context) {
        this.movieList = movieList;
        this.context = context;
        inflater = LayoutInflater.from(context);
        callBackMovie = (CallBackMovie) context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

      View view = inflater.inflate(R.layout.movie_list_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Movie movie = movieList.get(i);
        myViewHolder.tvTitle.setText(movie.getTitle());
        myViewHolder.tvDesc.setText(movie.getDescription());
        myViewHolder.imageView.setImageResource(movie.getImage());


    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvDesc;
        Button button;
        ImageView imageView;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDesc = itemView.findViewById(R.id.tvDes);
            imageView = itemView.findViewById(R.id.ivMovie);
            button = itemView.findViewById(R.id.btnDelete);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Movie movie=movieList.get(getAdapterPosition());
                    callBackMovie.delete(movie, getAdapterPosition());

                }
            });

        }
    }
}
