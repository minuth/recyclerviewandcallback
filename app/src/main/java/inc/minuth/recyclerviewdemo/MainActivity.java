package inc.minuth.recyclerviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import inc.minuth.recyclerviewdemo.Adapter.AdapterMovie;
import inc.minuth.recyclerviewdemo.AddMovieDialog.AddMovieAdapter;
import inc.minuth.recyclerviewdemo.CallBack.CallBackMovie;
import inc.minuth.recyclerviewdemo.Entity.Movie;

public class MainActivity extends AppCompatActivity implements CallBackMovie, CallBackMovie.AddDialogMovie {

    List<Movie> movieList;
    RecyclerView recyclerView;
    AdapterMovie adapterMovie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView =findViewById(R.id.rvMovie);
        movieList = new ArrayList<>();

        adapterMovie = new AdapterMovie(movieList, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapterMovie);

        getMovies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.addItem : {
                AddMovieAdapter addMovieAdapter = new AddMovieAdapter();
                addMovieAdapter.show(getSupportFragmentManager(),"Add New Movie");
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void getMovies() {
        for (int i=0; i<10; i++){
            movieList.add(new Movie("Khmer Movie"+i,"Good Movie for khmer",R.drawable.download));
        }
    }

    @Override
    public void delete(Movie movie, int position) {
        movieList.remove(movie);
        adapterMovie.notifyItemRemoved(position);
    }

    @Override
    public void getMovie(Movie movie) {
        movieList.add(0,movie);
        adapterMovie.notifyItemChanged(0);
    }
}
