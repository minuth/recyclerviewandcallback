package inc.minuth.recyclerviewdemo.AddMovieDialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import inc.minuth.recyclerviewdemo.CallBack.CallBackMovie;
import inc.minuth.recyclerviewdemo.Entity.Movie;
import inc.minuth.recyclerviewdemo.R;

public class AddMovieAdapter extends DialogFragment {


    CallBackMovie.AddDialogMovie addDialogMovie;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        addDialogMovie = (CallBackMovie.AddDialogMovie) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.add_movie_layout,null);
        builder.setTitle("Add New Movie");
        builder.setView(view);
        final MyViewHolder myViewHolder = new MyViewHolder(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Movie movie = new Movie(myViewHolder.edTitle.getText().toString(), myViewHolder.edDesc.getText().toString(),R.drawable.download);
                addDialogMovie.getMovie(movie);
            }
        });



        return builder.create();
    }


    class MyViewHolder{

        EditText edTitle, edDesc;

        public MyViewHolder(View view){
            edTitle= view.findViewById(R.id.edTitle);
            edDesc = view.findViewById(R.id.edDes);
        }

    }



}
