package inc.minuth.recyclerviewdemo.CallBack;

import inc.minuth.recyclerviewdemo.Entity.Movie;

public interface CallBackMovie {

    void delete(Movie movie, int position);

    interface AddDialogMovie{
        void getMovie(Movie movie);
    }

}
